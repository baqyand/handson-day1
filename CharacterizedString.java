import java.util.*;

class CharacterizedString{
	public static void main(String[] args){
		Scanner inp = new Scanner(System.in);

		
		System.out.println("Ketik kata/kalimat, lalu tekan enter : ");
		String input = inp.nextLine();
		System.out.println("-------------------------------");
		System.out.println("string penuh:" + input);
		System.out.println("-------------------------------");
		System.out.println("proses memecahkan kata/kalimat menjadi karakter...");
		System.out.println(".........");
		System.out.println("done.");
		System.out.println("------------------------------");

		
		for(int i = 0; i< input.length(); i++){
		System.out.println("Karakter ["+ i +"]" + ": " + input.charAt(i));
		}
	}	
}